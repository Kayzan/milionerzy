﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milionerzy
{
    public partial class Frm7 : Form
    {
        public Frm7()
        {
            InitializeComponent();
        }

        private void Btn1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Tak! Doskonale!");
            MessageBox.Show("Gratuluję! Jesteś teraz mistrzynią Teorii Literatury!");
            
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Niestety nie!");
            Close();
        }

        private void Btn3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Niestety nie!");
            Close();
        }

        private void Btn4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Niestety nie!");
            Close();
        }
    }
}
