﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milionerzy
{
    public partial class Frm4 : Form
    {
        public Frm4()
        {
            InitializeComponent();
        }

        private void Btn1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Niestety, ale to błędna odpowiedź!");
            Close();
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Niestety, ale to błędna odpowiedź!");
            Close();
        }

        private void Btn3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Masz rację!");
            Frm5 x4 = new Frm5();
            x4.Show();
        }

        private void Btn4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Niestety, ale to błędna odpowiedź!");
            Close();
        }
    }
}
