﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milionerzy
{
    public partial class Frm1 : Form
    {
        public Button Answer1 { get; }

        public Frm1()
        {
            InitializeComponent();
        
          
        }

        private void Frm1_Load(object sender, EventArgs e)
        {

        }

        private void Btn1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Niestety nie!");
            Close();
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Niestety nie!");
            Close();
        }

        private void Btn3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Masz rację!");
            Frm2 x1 = new Frm2();
            x1.Show();
        }

        private void Btn4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Niestety nie!");
            Close();
        }
    }
}
