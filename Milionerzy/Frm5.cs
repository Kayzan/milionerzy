﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milionerzy
{
    public partial class Frm5 : Form
    {
        public Frm5()
        {
            InitializeComponent();
        }

        private void Btn1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Tak, doskonale! Jest to definicja wg Tatarkiewicza!");
            Frn6 x5 = new Frn6();
            x5.Show();
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Niestety!");
            Close();
        }

        private void Btn3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Niestety!");
            Close();
        }

        private void Btn4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Niestety!");
            Close();
        }
    }
}
