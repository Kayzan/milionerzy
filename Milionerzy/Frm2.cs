﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milionerzy
{
    public partial class Frm2 : Form
    {
        public Frm2()
        {
            InitializeComponent();
        }

        private void Btn1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Nie bardzo!");
            Close();
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Nie bardzo!");
            Close();
        }

        private void Btn3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Nie bardzo!");
            Close();
        }

        private void Btn4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Świetnie!");
            Frm3 x2 = new Frm3();
            x2.Show();
        }
    }
}
