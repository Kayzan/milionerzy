﻿namespace Milionerzy
{
    partial class Frm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm1));
            this.Txt1 = new System.Windows.Forms.TextBox();
            this.Btn1 = new System.Windows.Forms.Button();
            this.Btn2 = new System.Windows.Forms.Button();
            this.Btn3 = new System.Windows.Forms.Button();
            this.Btn4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Txt1
            // 
            this.Txt1.Location = new System.Drawing.Point(89, 46);
            this.Txt1.Name = "Txt1";
            this.Txt1.Size = new System.Drawing.Size(441, 20);
            this.Txt1.TabIndex = 0;
            this.Txt1.Text = "Kto w \"Ferdydurke\" wielkim poetą był?";
            this.Txt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Btn1
            // 
            this.Btn1.Location = new System.Drawing.Point(42, 151);
            this.Btn1.Name = "Btn1";
            this.Btn1.Size = new System.Drawing.Size(224, 61);
            this.Btn1.TabIndex = 1;
            this.Btn1.Text = "A) Mickiewicz";
            this.Btn1.UseVisualStyleBackColor = true;
            this.Btn1.Click += new System.EventHandler(this.Btn1_Click);
            // 
            // Btn2
            // 
            this.Btn2.Location = new System.Drawing.Point(42, 305);
            this.Btn2.Name = "Btn2";
            this.Btn2.Size = new System.Drawing.Size(224, 61);
            this.Btn2.TabIndex = 2;
            this.Btn2.Text = "B) Gombrowicz";
            this.Btn2.UseVisualStyleBackColor = true;
            this.Btn2.Click += new System.EventHandler(this.Btn2_Click);
            // 
            // Btn3
            // 
            this.Btn3.Location = new System.Drawing.Point(362, 151);
            this.Btn3.Name = "Btn3";
            this.Btn3.Size = new System.Drawing.Size(224, 61);
            this.Btn3.TabIndex = 3;
            this.Btn3.Text = "C) Słowacki";
            this.Btn3.UseVisualStyleBackColor = true;
            this.Btn3.Click += new System.EventHandler(this.Btn3_Click);
            // 
            // Btn4
            // 
            this.Btn4.Location = new System.Drawing.Point(362, 305);
            this.Btn4.Name = "Btn4";
            this.Btn4.Size = new System.Drawing.Size(224, 61);
            this.Btn4.TabIndex = 4;
            this.Btn4.Text = "D) Kochanowski";
            this.Btn4.UseVisualStyleBackColor = true;
            this.Btn4.Click += new System.EventHandler(this.Btn4_Click);
            // 
            // Frm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(598, 467);
            this.Controls.Add(this.Btn4);
            this.Controls.Add(this.Btn3);
            this.Controls.Add(this.Btn2);
            this.Controls.Add(this.Btn1);
            this.Controls.Add(this.Txt1);
            this.Name = "Frm1";
            this.Text = "Pytanie 1";
            this.Load += new System.EventHandler(this.Frm1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Txt1;
        private System.Windows.Forms.Button Btn1;
        private System.Windows.Forms.Button Btn2;
        private System.Windows.Forms.Button Btn3;
        private System.Windows.Forms.Button Btn4;
    }
}