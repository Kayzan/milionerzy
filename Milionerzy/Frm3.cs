﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milionerzy
{
    public partial class Frm3 : Form
    {
        public Frm3()
        {
            InitializeComponent();
        }

        private void Btn1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Nie oni!");
            Close();
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Tak! To oni odkryli prawa harmonii dźwięków!");
            Frm4 x3 = new Frm4();
            x3.Show();
        }

        private void Btn3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("To nie oni!");
            Close();
        }

        private void Btn4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("To nie oni!");
            Close();
        }
    }
}
