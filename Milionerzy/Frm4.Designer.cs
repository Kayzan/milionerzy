﻿namespace Milionerzy
{
    partial class Frm4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm4));
            this.Lbl1 = new System.Windows.Forms.Label();
            this.Btn1 = new System.Windows.Forms.Button();
            this.Btn2 = new System.Windows.Forms.Button();
            this.Btn3 = new System.Windows.Forms.Button();
            this.Btn4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Lbl1
            // 
            this.Lbl1.AutoSize = true;
            this.Lbl1.Location = new System.Drawing.Point(98, 43);
            this.Lbl1.Name = "Lbl1";
            this.Lbl1.Size = new System.Drawing.Size(235, 13);
            this.Lbl1.TabIndex = 0;
            this.Lbl1.Text = "                             Dlaczego reguły były ważne?";
            this.Lbl1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Btn1
            // 
            this.Btn1.Location = new System.Drawing.Point(21, 120);
            this.Btn1.Name = "Btn1";
            this.Btn1.Size = new System.Drawing.Size(206, 63);
            this.Btn1.TabIndex = 1;
            this.Btn1.Text = "A) Ponieważ powstałby chaos";
            this.Btn1.UseVisualStyleBackColor = true;
            this.Btn1.Click += new System.EventHandler(this.Btn1_Click);
            // 
            // Btn2
            // 
            this.Btn2.Location = new System.Drawing.Point(21, 268);
            this.Btn2.Name = "Btn2";
            this.Btn2.Size = new System.Drawing.Size(206, 63);
            this.Btn2.TabIndex = 2;
            this.Btn2.Text = "B) Ponieważ ustalały zasady życia";
            this.Btn2.UseVisualStyleBackColor = true;
            this.Btn2.Click += new System.EventHandler(this.Btn2_Click);
            // 
            // Btn3
            // 
            this.Btn3.Location = new System.Drawing.Point(299, 120);
            this.Btn3.Name = "Btn3";
            this.Btn3.Size = new System.Drawing.Size(206, 63);
            this.Btn3.TabIndex = 3;
            this.Btn3.Text = "C) Ponieważ w starożytności sztuka była rzemiosłem";
            this.Btn3.UseVisualStyleBackColor = true;
            this.Btn3.Click += new System.EventHandler(this.Btn3_Click);
            // 
            // Btn4
            // 
            this.Btn4.Location = new System.Drawing.Point(299, 268);
            this.Btn4.Name = "Btn4";
            this.Btn4.Size = new System.Drawing.Size(206, 63);
            this.Btn4.TabIndex = 4;
            this.Btn4.Text = "D) Ponieważ ludzie w starożytności zajmowali się pisaniem reguł";
            this.Btn4.UseVisualStyleBackColor = true;
            this.Btn4.Click += new System.EventHandler(this.Btn4_Click);
            // 
            // Frm4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(517, 453);
            this.Controls.Add(this.Btn4);
            this.Controls.Add(this.Btn3);
            this.Controls.Add(this.Btn2);
            this.Controls.Add(this.Btn1);
            this.Controls.Add(this.Lbl1);
            this.Name = "Frm4";
            this.Text = "Frm4";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Lbl1;
        private System.Windows.Forms.Button Btn1;
        private System.Windows.Forms.Button Btn2;
        private System.Windows.Forms.Button Btn3;
        private System.Windows.Forms.Button Btn4;
    }
}